using Database;
using UnityEngine;
using Zenject;

public class PlaySceneInstaller : MonoInstaller<PlaySceneInstaller>
{
    [Header("Data")]
    [SerializeField] private LevelContainer _levelContainer;

    [Header("Database")]
    [SerializeField] private Transform _statsRowsParent;
    [SerializeField] private GameObject _rowPrefab;

    public override void InstallBindings()
    {
        Container.BindInstance(_levelContainer).AsSingle();
        Container.Bind<SceneController>().AsSingle();
        Container.Bind<IDatabaseBroker>().To<SqliteDBBroker>().AsSingle();
        Container.Bind<DatabaseService>().AsSingle();
        Container.Bind<StatsRowFactory>().AsSingle();
        Container.BindInstance(_statsRowsParent).AsSingle().WhenInjectedInto(typeof(StatsRowFactory));
        Container.Bind<StatsManager>().AsSingle();
        Container.BindInstance(_rowPrefab).WhenInjectedInto(typeof(StatsManager));
    }
}