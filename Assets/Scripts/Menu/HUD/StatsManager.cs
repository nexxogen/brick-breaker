﻿using Database;
using System.Collections.Generic;
using UnityEngine;

public class StatsManager 
{
    // Injected variables
    private DatabaseService _databaseService;
    private StatsRowFactory _factory;
    private GameObject _rowPrefab;

    // Private variables
    private List<THighScore> _highScores;

    public StatsManager(DatabaseService databaseService, 
		StatsRowFactory factory, 
		GameObject rowPrefab)
    {
        _databaseService = databaseService;
        _factory = factory;
        _rowPrefab = rowPrefab;
    }

    public void PopulateStats()
    {
        _highScores = _databaseService.GetHighScores();

        for (int i = 0; i < _highScores.Count; i++)
        {
            _factory.Prefab = _rowPrefab;

            StatsRow row = _factory.Create();
            row.Populate((_highScores[i].Level + 1).ToString(), _highScores[i].Score.ToString(), _highScores[i].TimesPlayed.ToString());
        }
    }
}