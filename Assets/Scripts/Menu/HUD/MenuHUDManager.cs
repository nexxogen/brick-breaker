﻿using UnityEngine;
using Zenject;

public class MenuHUDManager : MonoBehaviour
{
    // Injected variables
    private LevelContainer _levelContainer;
    private SceneController _sceneController;
    private StatsManager _statsManager;

    [Inject]
    public void Construct(LevelContainer levelContainer, 
		SceneController sceneController, 
		StatsManager statsManager)
    {
        _levelContainer = levelContainer;
        _sceneController = sceneController;
        _statsManager = statsManager;
    }

    /// <summary>
    /// OnClick event
    /// </summary>
    public void ExitGame()
    {
        Application.Quit();
    }

    /// <summary>
    /// OnClick event
    /// </summary>
    /// <param name="level"></param>
    public void PlayLevel(int level)
    {
        _levelContainer.CurrentlyLoaded = level;
        _sceneController.LoadScene(Constants.Scenes.GAME);
    }

    /// <summary>
    /// OnClick event
    /// </summary>
    public void PopulateStats()
    {
        _statsManager.PopulateStats();
    }
}