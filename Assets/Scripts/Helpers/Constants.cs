﻿public static class Constants
{
    public struct Tags
    {
        public const string PAD = "Pad";
        public const string BALL = "Ball";
        public const string BOTTOM = "Bottom";
        public const string BRICK = "Brick";
    }

    public struct Scenes
    {
        public const string MENU = "Menu";
        public const string GAME = "Game";
    }
}