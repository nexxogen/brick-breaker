﻿using UnityEngine.SceneManagement;

public class SceneController
{
    private string _currentSceneName;

    public string CurrentSceneName
    {
        get { return _currentSceneName; }
    }

    public SceneController()
    {
        SceneManager.activeSceneChanged += (p, c) => _currentSceneName = c.name;
    }

    public void LoadScene(string sceneName)
    {
        SceneManager.LoadSceneAsync(sceneName);
    }

    public void ReloadScene()
    {
        SceneManager.LoadSceneAsync(_currentSceneName);
    }
}