﻿using UnityEngine;

[CreateAssetMenu(fileName = "Level Prefabs Container", menuName = "Scriptable Objects/Level Prefabs Container")]
public class LevelPrefabsContainer : ScriptableObject
{
    [SerializeField] private GameObject[] _powerUps;
    [SerializeField] private GameObject _brick;

    public GameObject[] PowerUps { get { return _powerUps; } }
    public GameObject Brick { get { return _brick; } }
}