﻿using UnityEngine;

public class LevelSpawner : ILevelSpawner
{
    // Injected variables
    private LevelContainer _container;
    private LevelSettings _settings;
    private BrickFactory _brickFactory;
    private GameManager _gameManager;

    // Local variables
    private Vector3 _brickExtents;
    private float _positionX;
    private float _positionY;
    private int _multiplier;
    private int _side;

    public LevelSpawner(LevelContainer container, 
		LevelPrefabsContainer prefabs, 
		LevelSettings settings, 
		BrickFactory factory, 
		GameManager gameManager)
    {
        _container = container;
        _settings = settings;
        _brickFactory = factory;
        _brickFactory.Prefab = prefabs.Brick;
        _gameManager = gameManager;

        _positionY = _settings.Height;
        _multiplier = 1;
    }

    public void SpawnLevel(int index)
    {
        Level level = _container.GetLevel(index);

        for (int i = 0; i < level.Array.GetLength(0); i++)
        {
            for (int j = 0; j < level.Array.GetLength(1); j++)
            {
                if (level.Array[i, j] != 0)
                {
                    Brick brick = _brickFactory.Create();

                    brick.Hits = level.Array[i, j];
                    _brickExtents = brick.SpriteRenderer.bounds.extents;
                    _positionX = CalculateNextX(j, _brickExtents.x);

                    brick.transform.position = new Vector3(_positionX, _positionY);
                    _gameManager.BricksCount++;
                }
                else
                {
                    _positionX = CalculateNextX(j, _brickExtents.x);
                }
            }

            _positionY -= 2 * _brickExtents.y;
            _multiplier = 1;
        }
    }

    private float CalculateNextX(int arrayIndex, float extentsX)
    {
        _multiplier = arrayIndex % 2 == 0 && arrayIndex > 1 ? _multiplier + 2 : _multiplier;
        _side = arrayIndex % 2 == 0 ? -1 : 1;

        return _side * _multiplier * extentsX;
    }
}