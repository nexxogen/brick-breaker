﻿using UnityEngine;
using Zenject;

public class LevelSpawnerBehaviour : MonoBehaviour 
{
    private ILevelSpawner _spawner;
    private LevelContainer _container;

    [Inject]
    private void Construct(ILevelSpawner spawner, LevelContainer container)
    {
        _spawner = spawner;
        _container = container;
    }

    private void Start()
    {
        _spawner.SpawnLevel(_container.CurrentlyLoaded);
    }
}