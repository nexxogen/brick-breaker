﻿using UnityEngine;

[CreateAssetMenu(fileName = "Level Container", menuName = "Scriptable Objects/Level Container")]
public class LevelContainer : ScriptableObject 
{
    [SerializeField] private Level[] _levels;
    [SerializeField] private int _currentlyLoaded;

    public Level[] Levels { get { return _levels; } }
    public int CurrentlyLoaded { get { return _currentlyLoaded; } set { _currentlyLoaded = value; } }

    public Level GetLevel(int index)
    {
        CurrentlyLoaded = index;
        return _levels[index];
    }
}
