﻿using UnityEngine;

[CreateAssetMenu(fileName = "Level Settings", menuName = "Scriptable Objects/Level Settings")]
public class LevelSettings : ScriptableObject
{
    [SerializeField] private float _height;

    public float Height { get { return _height; } }
}