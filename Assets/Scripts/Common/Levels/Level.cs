﻿using UnityEngine;

[CreateAssetMenu(fileName = "Level", menuName = "Scriptable Objects/Level")]
public class Level : ScriptableObject
{
    [SerializeField] private TextAsset _csv;

    private int[,] _array;
    public int[,] Array { get { return _array; } }

    private void OnEnable()
    {
        _array = ParseCSV(_csv);
        _array = TransformArray(_array);
    }

    private int[,] ParseCSV(TextAsset csv)
    {
        string text = csv.text;
        text = text.Replace('\n', '\r');

        string[] lines = text.Split(new char[] { '\r' }, System.StringSplitOptions.RemoveEmptyEntries);

        int numberOfRows = lines.Length;
        int numberOfColumns = lines[0].Split(',').Length;

        int[,] array = new int[numberOfRows, numberOfColumns];

        for (int i = 0; i < numberOfRows; i++)
        {
            string[] line = lines[i].Split(',');
            for (int j = 0; j < numberOfColumns; j++)
            {
                array[i, j] = int.Parse(line[j]);
            }
        }

        return array;
    }
    
    private int[,] TransformArray(int[,] array)
    {
        int rows = array.GetLength(0);
        int columns = array.GetLength(1);

        int[,] newArray = new int[rows, columns];

        for (int i = 0; i < newArray.GetLength(0); i++)
        {
            newArray[i, 0] = array[i, 7];
            newArray[i, 1] = array[i, 8];
            newArray[i, 2] = array[i, 6];
            newArray[i, 3] = array[i, 9];
            newArray[i, 4] = array[i, 5];
            newArray[i, 5] = array[i, 10];
            newArray[i, 6] = array[i, 4];
            newArray[i, 7] = array[i, 11];
            newArray[i, 8] = array[i, 3];
            newArray[i, 9] = array[i, 12];
            newArray[i, 10] = array[i, 2];
            newArray[i, 11] = array[i, 13];
            newArray[i, 12] = array[i, 1];
            newArray[i, 13] = array[i, 14];
            newArray[i, 14] = array[i, 0];
            newArray[i, 15] = array[i, 15];
        }

        return newArray;
    }
}