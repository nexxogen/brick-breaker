﻿public interface ILevelSpawner
{
    void SpawnLevel(int level);
}