﻿using UnityEngine;
using Zenject;

public class StatsRowFactory : IFactory<StatsRow>
{
    // Injected variables
    private readonly DiContainer _container;
    private Transform _parent;

    // Local variables and properties
    private GameObject _prefab;
    public GameObject Prefab { set { _prefab = value; } }

    public StatsRowFactory(DiContainer container, Transform parent)
    {
        _container = container;
        _parent = parent;
    }

    public StatsRow Create()
    {
        GameObject statsRow = Object.Instantiate(_prefab, _parent);
        _container.InjectGameObjectForComponent<StatsRow>(statsRow);

        StatsRow statsRowComponent = statsRow.GetComponent<StatsRow>();

        return statsRowComponent;
    }
}
