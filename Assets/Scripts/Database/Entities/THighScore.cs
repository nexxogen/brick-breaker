﻿using System.Collections.Generic;
using System.Data;

namespace Database
{
    public class THighScore : IDatabaseEntity
    {
        public int ID { get; set; }
        public int Level { get; set; }
        public int Score { get; set; }
        public int TimesPlayed { get; set; }

        public string Name { get { return "HighScores"; } }
        public string InsertValues { get { return string.Format("(Level, Score, TimesPlayed) VALUES ({0}, {1}, {2})", Level, Score, TimesPlayed); } }
        public string UpdateValues { get { return string.Format("Score = {0}, TimesPlayed = {1}", Score, TimesPlayed); } }
        public string UpdateCondition { get { return string.Format("Level = {0}", Level); } }

        public THighScore() { }
        public THighScore(int id, int level, int score, int timesPlayed)
        {
            ID = id;
            Level = level;
            Score = score;
            TimesPlayed = timesPlayed;
        }

        public List<IDatabaseEntity> FillObjectList(IDataReader reader)
        {
            List<IDatabaseEntity> result = new List<IDatabaseEntity>();

            while (reader.Read())
            {
                result.Add(new THighScore(
                    reader.GetInt32(0),
                    reader.GetInt32(1),
                    reader.GetInt32(2),
                    reader.GetInt32(3)));
            }

            return result;
        }

    }
}
