﻿using System.Collections.Generic;
using System.Data;

namespace Database
{
    public interface IDatabaseEntity
    {
        string Name { get; }
        List<IDatabaseEntity> FillObjectList(IDataReader reader);
        string InsertValues { get; }
        string UpdateValues { get; }
        string UpdateCondition { get; }
    }
}