﻿using Database;
using System.Collections.Generic;

public class DatabaseService
{
	// Injected variables
    private IDatabaseBroker _broker;
    private List<THighScore> _currentScores;

    public DatabaseService(IDatabaseBroker broker, List<THighScore> currentScores)
    {
        _broker = broker;
        _currentScores = GetHighScores();
    }

    public List<THighScore> GetHighScores()
    {
        return _broker.SelectAll(new THighScore()).ConvertAll(x => (THighScore)x);
    }

    public void UpdateHighScores(int level, int score)
    {
        THighScore highScore = new THighScore();

        THighScore currentForLevel = _currentScores.Find(s => s.Level == level);

        if (currentForLevel.Score > score)
        {
            highScore.Score = currentForLevel.Score;
        }
        else
        {
            highScore.Score = score;
        }

        highScore.TimesPlayed = ++currentForLevel.TimesPlayed;
        highScore.Level = level;

        _broker.Update(highScore);
    }
}