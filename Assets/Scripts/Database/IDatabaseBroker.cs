﻿using System.Collections.Generic;

namespace Database
{
    public interface IDatabaseBroker
    {
        List<IDatabaseEntity> SelectAll(IDatabaseEntity entity);
        void Update(IDatabaseEntity entity);
    }
}