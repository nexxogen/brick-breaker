﻿using UnityEngine;
using Zenject;

public class DatabaseBehaviour : MonoBehaviour
{
    // Injected variables
    private DatabaseService _databaseService;
    private GameManager _gameManager;
    private LevelContainer _levelContainer;

    [Inject]
    private void Construct(DatabaseService databaseService, 
		GameManager gameManager, 
		LevelContainer levelContainer)
    {
        _databaseService = databaseService;
        _gameManager = gameManager;
        _levelContainer = levelContainer;
    }

    /// <summary>
    /// Event action
    /// </summary>
    public void UpdateHighScores()
    {
        int score = _gameManager.Score;
        int level = _levelContainer.CurrentlyLoaded;

        _databaseService.UpdateHighScores(level, score);
    }
}