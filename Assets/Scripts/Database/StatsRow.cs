﻿using UnityEngine;
using UnityEngine.UI;

public class StatsRow : MonoBehaviour
{
    [SerializeField] private Text _level;
    [SerializeField] private Text _score;
    [SerializeField] private Text _timesPlayed;

    public void Populate(string level, string score, string timesPlayed)
    {
        _level.text = level;
        _score.text = score;
        _timesPlayed.text = timesPlayed;
    }
}