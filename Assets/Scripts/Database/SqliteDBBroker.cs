﻿using Mono.Data.Sqlite;
using System.Collections.Generic;
using System.Data;
using UnityEngine;

namespace Database
{
    public class SqliteDBBroker : IDatabaseBroker
    {
        private string _connectionString;

        public SqliteDBBroker()
        {
            _connectionString = "URI=file:" + Application.dataPath + "/Database/BrickBreakerDB.sqlite";
        }

        public List<IDatabaseEntity> SelectAll(IDatabaseEntity entity)
        {
            List<IDatabaseEntity> result;

            using (IDbConnection connection = new SqliteConnection(_connectionString))
            {
                connection.Open();

                using (IDbCommand command = connection.CreateCommand())
                {
                    string sql = "SELECT * FROM " + entity.Name;
                    command.CommandText = sql;

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        result = entity.FillObjectList(reader);

                        connection.Close();
                        reader.Close();
                    }
                }
            }

            return result;
        }

        public void Update(IDatabaseEntity entity)
        {
            using (IDbConnection connection = new SqliteConnection(_connectionString))
            {
                connection.Open();

                using (IDbCommand command = connection.CreateCommand())
                {
                    string sql = string.Format("UPDATE {0} SET {1} WHERE {2}", entity.Name, entity.UpdateValues, entity.UpdateCondition);

                    command.CommandText = sql;
                    command.ExecuteNonQuery();

                    connection.Close();
                }
            }
        }
    }
}