﻿using System.Collections.Generic;
using UnityEngine;

public class BrickManager
{
    private List<GameObject> _bricks;
    private int _count;

    public BrickManager(List<GameObject> bricks)
    {
        _bricks = bricks;
        _count = _bricks.Count;
    }

    /// <summary>
    /// Event action
    /// </summary>
    public void OnBrickDestroyed()
    {
        _count--;
    }
}