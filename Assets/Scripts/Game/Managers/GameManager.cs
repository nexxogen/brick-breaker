﻿using UnityEngine;
using UnityEngine.Events;
using Zenject;

public class GameManager : MonoBehaviour
{
    [SerializeField] private UnityEvent _gameLost;
    [SerializeField] private UnityEvent _gameWon;
    [SerializeField] private UnityEvent _scoreUpdated;
    [SerializeField] private UnityEvent _livesUpdated;

    // Injected variables
    private GameSettings _settings;

    // Local variables
    private int _currentLives;
    private int _bricksCount;
    private int _score = 0;

    public int BricksCount { get { return _bricksCount; } set { _bricksCount = value; } }
    public int CurrentLives { get { return _currentLives; } }
    public int Score { get { return _score; } }

    [Inject]
    public void Construct(GameSettings settings)
    {
        _settings = settings;
        _currentLives = _settings.MaximumLives;
    }

    /// <summary>
    /// Event action
    /// </summary>
    public void HandleBallHitBottom()
    {
        _currentLives--;
        _livesUpdated.Invoke();

        if (_currentLives == 0)
        {
            _gameLost.Invoke();
        }
    }

    /// <summary>
    /// Event action
    /// </summary>
    public void HandleBrickDestroyed()
    {
        _bricksCount--;
        _score += _settings.BrickScore;
        _scoreUpdated.Invoke();

        if (_bricksCount == 0)
        {
            _gameWon.Invoke();
        }
    }

    /// <summary>
    /// Event action
    /// </summary>
    public void HandlePowerUpHit()
    {
        _score += _settings.PowerUpScore;
        _scoreUpdated.Invoke();
    }
}
