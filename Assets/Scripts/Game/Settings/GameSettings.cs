﻿using UnityEngine;

[CreateAssetMenu(fileName = "Game Settings", menuName = "Scriptable Objects/Game Settings")]
public class GameSettings : ScriptableObject
{
    [Header("Ball")]
    [SerializeField] private float _slowBallSpeed;
    [SerializeField] private float _regularBallSpeed;

    [Header("Pad")]
    [SerializeField] private float _padSpeed;

    [Header("Power Ups")]
    [SerializeField] private float _enlargeAmount;
    [SerializeField] private float _maximumScale;

    [Header("Game")]
    [SerializeField] private int _maximumLives;
    [SerializeField] private int _brickScore;
    [SerializeField] private int _powerUpScore;

    public float SlowBallSpeed { get { return _slowBallSpeed; } }
    public float RegularBallSpeed { get { return _regularBallSpeed; } }
    public float PadSpeed { get { return _padSpeed; } }
    public float EnlargeAmount { get { return _enlargeAmount; } }
    public float MaximumScale { get { return _maximumScale; } }
    public int MaximumLives { get { return _maximumLives; } }
    public int BrickScore { get { return _brickScore; } }
    public int PowerUpScore { get { return _powerUpScore; } }
}