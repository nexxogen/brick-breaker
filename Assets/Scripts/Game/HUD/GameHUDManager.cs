﻿using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class GameHUDManager : MonoBehaviour
{
    // Injected variables and properties
    private GameManager _gameManager;
    private SceneController _sceneController;
    private LevelContainer _levelContainer;

    [Inject(Id = GameSceneReferencesInstaller.ID_SCORE)] private Text Score { get; set; }
    [Inject(Id = GameSceneReferencesInstaller.ID_LIVES)] private Text Lives { get; set; }
	[Inject(Id = GameSceneReferencesInstaller.ID_GAME_START_MESSAGE)] private GameObject GameStartMessage { get; set; }
    [Inject(Id = GameSceneReferencesInstaller.ID_GAME_OVER_POPUP)] private GameObject GameOverPopup { get; set; }
    [Inject(Id = GameSceneReferencesInstaller.ID_GAME_LOST)] private GameObject GameLost { get; set; }
    [Inject(Id = GameSceneReferencesInstaller.ID_GAME_WON)] private GameObject GameWon { get; set; }
    [Inject(Id = GameSceneReferencesInstaller.ID_NEXT_BUTTON)] private GameObject NextButton { get; set; }

    [Inject]
    public void Construct(GameManager gameManager, 
		SceneController sceneController, 
		LevelContainer levelContainer)
    {
        _gameManager = gameManager;
        _sceneController = sceneController;
        _levelContainer = levelContainer;
    }

	/// <summary>
	/// Event action
	/// </summary>
	public void DisableGameStartMessage()
	{
		GameStartMessage.SetActive(false);
	}

    /// <summary>
    /// Event action
    /// </summary>
    public void UpdateScore()
    {
        Score.text = _gameManager.Score.ToString();
    }

    /// <summary>
    /// Event action
    /// </summary>
    public void UpdateLives()
    {
        Lives.text = _gameManager.CurrentLives.ToString();
    }

    /// <summary>
    /// Event action
    /// </summary>
    public void HandleGameLost()
    {
        GameOverPopup.SetActive(true);
        GameLost.SetActive(true);
    }

    /// <summary>
    /// Event action
    /// </summary>
    public void HandleGameWon()
    {
        GameOverPopup.SetActive(true);

        if (_levelContainer.CurrentlyLoaded == _levelContainer.Levels.Length - 1)
        {
            NextButton.SetActive(false);
        }

        GameWon.SetActive(true);
    }

    /// <summary>
    /// OnClick event
    /// </summary>
    public void ReloadScene()
    {
        _sceneController.ReloadScene();
    }

    /// <summary>
    /// OnClick event
    /// </summary>
    public void LoadMenuScene()
    {
        _sceneController.LoadScene(Constants.Scenes.MENU);
    }

    /// <summary>
    /// OnClick event
    /// </summary>
    public void LoadNextLevel()
    {
        if (_levelContainer.CurrentlyLoaded < _levelContainer.Levels.Length - 1)
        {
            _levelContainer.CurrentlyLoaded++;
            _sceneController.ReloadScene();
        }
    }
}

