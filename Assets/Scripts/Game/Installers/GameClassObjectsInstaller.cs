using Database;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class GameClassObjectsInstaller : MonoInstaller<GameClassObjectsInstaller>
{
    public override void InstallBindings()
    {
        Container.Bind<List<GameObject>>().AsTransient();
        Container.Bind<SpriteRenderer>().FromComponentSibling().AsTransient();
        Container.Bind<Rigidbody2D>().FromComponentSibling().AsTransient();
        Container.Bind<IInputHandler>().To<KeyboardInputHandler>().AsSingle();
        Container.Bind<BrickFactory>().AsTransient();
        Container.Bind<PowerUpFactory>().AsTransient();
        Container.Bind<SceneController>().AsSingle();
        Container.Bind<IDatabaseBroker>().To<SqliteDBBroker>().AsSingle();
        Container.Bind<DatabaseService>().AsSingle();
        Container.Bind<ILevelSpawner>().To<LevelSpawner>().AsSingle();
        Container.Bind<PowerUpSpawner>().AsSingle();
    }
}