using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class GameSceneReferencesInstaller : MonoInstaller<GameSceneReferencesInstaller>
{
    // Boundaries
    public const string ID_LEFT_WALL = "Left Wall";
    public const string ID_RIGHT_WALL = "Right Wall";
    public const string ID_BOTTOM = "Bottom";

    public const string ID_LEFT_WALL_RENDERER = "Left Wall Sprite Renderer";
    public const string ID_RIGHT_WALL_RENDERER = "Right Wall Sprite Renderer";

    // HUD
    public const string ID_SCORE = "Score";
    public const string ID_LIVES = "Lives Value";
	public const string ID_GAME_START_MESSAGE = "Game Start Message";
    public const string ID_GAME_OVER_POPUP = "Game Over Popup";
    public const string ID_GAME_LOST = "Game Lost";
    public const string ID_GAME_WON = "Game Won";
    public const string ID_NEXT_BUTTON = "Next Button";

    [Header("Boundaries")]
    [SerializeField] private GameObject _leftWall;
    [SerializeField] private GameObject _rightWall;
    [SerializeField] private GameObject _bottom;

    [Header("Game Elements")]
    [SerializeField] private GameObject _pad;
    [SerializeField] private Ball _ball;

    [Header("Parents")]
    [SerializeField] private Transform _bricksParent;
    [SerializeField] private Transform _powerUpsParent;

    [Header("Managers")]
    [SerializeField] private GameManager _gameManager;

    [Header("Data")]
    [SerializeField] private LevelContainer _levelContainer;
    [SerializeField] private LevelPrefabsContainer _levelPrefabsContainer;

    [Header("Settings")]
    [SerializeField] private GameSettings _gameSettings;
    [SerializeField] private LevelSettings _levelSettings;

    [Header("HUD")]
    [SerializeField] private Text _score;
    [SerializeField] private Text _lives;
	[SerializeField] private GameObject _gameStartMessage;
    [SerializeField] private GameObject _gameOverPopup;
    [SerializeField] private GameObject _gameWon;
    [SerializeField] private GameObject _gameLost;
    [SerializeField] private GameObject _nextButton;

    public override void InstallBindings()
    {
        // Bounderies
        Container.BindInstance(_leftWall).WithId(ID_LEFT_WALL);
        Container.BindInstance(_rightWall).WithId(ID_RIGHT_WALL);
        Container.BindInstance(_bottom).WithId(ID_BOTTOM);
        Container.Bind<SpriteRenderer>().WithId(ID_LEFT_WALL_RENDERER).FromMethod(c => _leftWall.GetComponent<SpriteRenderer>());
        Container.Bind<SpriteRenderer>().WithId(ID_RIGHT_WALL_RENDERER).FromMethod(c => _rightWall.GetComponent<SpriteRenderer>());

        // Game Elements
        Container.Bind<Transform>().FromMethod(c => _pad.transform).WhenInjectedInto(typeof(Ball));
        Container.Bind<Bounds>().FromMethod(c => _pad.GetComponent<SpriteRenderer>().bounds).WhenInjectedInto(typeof(Ball));
        Container.BindInstance(_ball).AsSingle();
        
        // Parents
        Container.BindInstance(_powerUpsParent).WhenInjectedInto(typeof(PowerUpFactory));
        Container.BindInstance(_bricksParent).WhenInjectedInto(typeof(BrickFactory));

        // Managers
        Container.BindInstance(_gameManager).AsSingle();

        // Data
        Container.BindInstance(_levelContainer).AsSingle();
        Container.BindInstance(_levelPrefabsContainer).AsSingle();

        // Settings
        Container.BindInstance(_gameSettings).AsSingle();
        Container.BindInstance(_levelSettings).AsSingle();

        // HUD
        Container.BindInstance(_score).WithId(ID_SCORE);
        Container.BindInstance(_lives).WithId(ID_LIVES);
		Container.BindInstance(_gameStartMessage).WithId(ID_GAME_START_MESSAGE);
        Container.BindInstance(_gameOverPopup).WithId(ID_GAME_OVER_POPUP);
        Container.BindInstance(_gameLost).WithId(ID_GAME_LOST);
        Container.BindInstance(_gameWon).WithId(ID_GAME_WON);
        Container.BindInstance(_nextButton).WithId(ID_NEXT_BUTTON);
    }
}