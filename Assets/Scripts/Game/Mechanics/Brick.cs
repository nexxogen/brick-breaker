﻿using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(SpriteRenderer))]
public class Brick : MonoBehaviour
{
    [SerializeField, Range(1, 4)] private int _hits;
    [SerializeField, Range(0, 1)] private float _powerUpProbability;
    [SerializeField] private Color[] _colors;
    [Space(20), SerializeField] private UnityEvent _brickDestroyed;

    private SpriteRenderer _spriteRenderer;
    private PowerUpSpawner _powerUpSpawner;
    private int _counter = 0;

    public int Hits { set { _hits = value; } }
    public SpriteRenderer SpriteRenderer { get { return _spriteRenderer; } set { _spriteRenderer = value; } }
    public PowerUpSpawner PowerUpSpawner { set { _powerUpSpawner = value; } }

    private void Start()
    {
        SetColor(_hits - 1, _colors);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag(Constants.Tags.BALL))
        {
            _counter++;

            if (_counter < _hits)
            {
                SetColor(_hits - _counter - 1, _colors);
            }
            else
            {
                _brickDestroyed.Invoke();
                _powerUpSpawner.Spawn(transform.position, _powerUpProbability); 
                gameObject.SetActive(false);
            }
        }
    }

    private void SetColor(int hit, Color[] colors)
    {
        _spriteRenderer.color = colors[hit];
    }
}