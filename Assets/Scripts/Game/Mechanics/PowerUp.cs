﻿using UnityEngine;
using UnityEngine.Events;
using Zenject;

[RequireComponent(typeof(Rigidbody2D))]
public class PowerUp : MonoBehaviour
{
    [SerializeField] private UnityEvent _hitPad;
    private Rigidbody2D _rigidBody;

    [Inject]
    private void Construct(Rigidbody2D rigidbody)
    {
        _rigidBody = rigidbody;
    }

    public void Launch(Vector3 position)
    {
        transform.position = position;

        int side = position.x >= 0 ? -1 : 1;
        _rigidBody.AddForce(new Vector2(side * 90, 150));
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag(Constants.Tags.PAD))
        {
            _hitPad.Invoke();
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag(Constants.Tags.BOTTOM))
        {
            Destroy(gameObject);
        }
    }
}

