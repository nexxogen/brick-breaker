﻿using UnityEngine;
using Zenject;

public class PowerUpFactory : IFactory<PowerUp>
{
	// Injected variables
    private readonly DiContainer _container;
    private Transform _parent;

	// Local variables and properties
    private GameObject _prefab;
    public GameObject Prefab { get { return _prefab; } set { _prefab = value; } }

    public PowerUpFactory(DiContainer container, Transform parent)
    {
        _container = container;
        _parent = parent;
    }

    public PowerUp Create()
    {
        GameObject powerUp = Object.Instantiate(_prefab, _parent);
        _container.InjectGameObjectForComponent<PowerUp>(powerUp);

        return powerUp.GetComponent<PowerUp>();
    }
}