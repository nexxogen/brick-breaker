﻿using UnityEngine;
using Zenject;

public class BrickFactory : IFactory<Brick>
{
    private readonly DiContainer _container;
    private Transform _parent;

    private GameObject _prefab;
    private PowerUpSpawner _powerUpSpawner;

    public GameObject Prefab { set { _prefab = value; } }

    public BrickFactory(DiContainer container, 
		Transform parent, 
		PowerUpSpawner powerUpSpawner)
    {
        _container = container;
        _parent = parent;
        _powerUpSpawner = powerUpSpawner;
    }

    public Brick Create()
    {
        GameObject brick = Object.Instantiate(_prefab, _parent);
        _container.InjectGameObjectForComponent<Brick>(brick);

        Brick brickComponent = brick.GetComponent<Brick>();

        brickComponent.SpriteRenderer = brick.GetComponent<SpriteRenderer>();
        brickComponent.PowerUpSpawner = _powerUpSpawner;

        return brickComponent;
    }
}