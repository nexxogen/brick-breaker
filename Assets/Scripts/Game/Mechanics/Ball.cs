﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using Zenject;

public class Ball : MonoBehaviour 
{
    [SerializeField] private UnityEvent _hitBottom;

    // Injected properties
    private Rigidbody2D _rigidBody;
    private Bounds _padBounds;
    private GameSettings _settings;
    private Transform _padTransform;
    private GameManager _gameManager;

    // Local properties
    private Vector2 _contactPoint;
    private float _clampedY;
    private int _side;

    private bool _isSlowedDown = false;
    private bool _isOnPad = true;

    private float Speed { get { return _isSlowedDown ? _settings.SlowBallSpeed : _settings.RegularBallSpeed; } }
    public bool IsOnPad { get { return _isOnPad; } }

    [Inject]
    private void Construct(Rigidbody2D rigidBody, 
		Bounds padBounds, 
		GameSettings settings, 
		Transform padTransform, 
		GameManager gameManager)
    {
        _rigidBody = rigidBody;
        _padBounds = padBounds;
        _settings = settings;
        _padTransform = padTransform;
        _gameManager = gameManager;
    }

    private void Update()
    {
        if (_isOnPad)
        {
            transform.position = new Vector3(_padTransform.position.x, _padTransform.position.y + 0.3f);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag(Constants.Tags.BOTTOM))
        {
            _hitBottom.Invoke();

            if (_gameManager.CurrentLives > 0)
            {
                AttachToPad();
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag(Constants.Tags.PAD))
        {
            if (collision.contacts.Length > 0)
            {
                _padBounds = collision.collider.GetComponent<SpriteRenderer>().bounds;
                _contactPoint = collision.contacts[0].point;

                if (_contactPoint.x < _padBounds.center.x)
                {
                    _side = -1;
                }
                else if (_contactPoint.x > _padBounds.center.x)
                {
                    _side = 1;
                }

                _rigidBody.velocity = new Vector2(_side * 5.0f * Mathf.Clamp01(Mathf.Abs(_contactPoint.x - _padBounds.center.x)), _rigidBody.velocity.y);

                if (_rigidBody.velocity.magnitude != Speed)
                {
                    ClampSpeed();
                }
            }
        }
    }

    private void ClampSpeed()
    {
        _rigidBody.velocity = Vector2.ClampMagnitude(_rigidBody.velocity * 10, Speed);
    }

    private void AttachToPad()
    {
        _isOnPad = true;
        _rigidBody.velocity = Vector2.zero;
    }

    /// <summary>
    /// Event action
    /// </summary>
    public void Launch()
    {
        int random = Random.Range(0, 2);
        int side = random == 0 ? -1 : 1;

        _isOnPad = false;
        _rigidBody.AddForce(new Vector2(side * 100, 200));
    }

    /// <summary>
    /// Event action
    /// </summary>
    public void SlowDownTemporarily()
    {
        StartCoroutine(SlowDownCoroutine(10));
    }

    private IEnumerator SlowDownCoroutine(float duration)
    {
        if (!_isSlowedDown)
        {
            _isSlowedDown = true;
            _rigidBody.velocity = Vector2.ClampMagnitude(_rigidBody.velocity * 10, Speed);

            yield return new WaitForSeconds(duration);

            _isSlowedDown = false;
            _rigidBody.velocity = Vector2.ClampMagnitude(_rigidBody.velocity * 10, Speed);
        }
    }

    /// <summary>
    /// Event action
    /// </summary>
    public void Destroy()
    {
        Destroy(gameObject);
    }
}