﻿using UnityEngine;
using UnityEngine.Events;
using Zenject;

public class PadController : MonoBehaviour
{
    [SerializeField] private UnityEvent _launchBall;

    // Injected properties
    [Inject(Id = GameSceneReferencesInstaller.ID_LEFT_WALL)] private GameObject LeftWall { get; set; }
    [Inject(Id = GameSceneReferencesInstaller.ID_RIGHT_WALL)] private GameObject RightWall { get; set; }
    [Inject(Id = GameSceneReferencesInstaller.ID_LEFT_WALL_RENDERER)] private SpriteRenderer LeftWallSpriteRenderer { get; set; }
    [Inject(Id = GameSceneReferencesInstaller.ID_RIGHT_WALL_RENDERER)] private SpriteRenderer RightWallSpriteRenderer { get; set; }

    // Injected variables
    private IInputHandler _input;
    private SpriteRenderer _spriteRenderer;
    private Ball _ball;
    private GameSettings _settings;

    // Local variables
    private Vector3 _position;
    private float _leftEdge;
    private float _rightEdge;

    [Inject]
    private void Construct(SpriteRenderer spriteRenderer, 
		IInputHandler input, 
		GameSettings settings, 
		Ball ball)
    {
        _input = input;
        _spriteRenderer = spriteRenderer;
        _settings = settings;
        _ball = ball;
    }

    private void Start()
    {
        _leftEdge = GetLeftEdge();
        _rightEdge = GetRightEdge();
    }

    private void Update()
    {
        if (_input.GetLeft())
        {
            transform.position -= new Vector3(_settings.PadSpeed, 0) * Time.deltaTime;
        }
        else if (_input.GetRight())
        {
            transform.position += new Vector3(_settings.PadSpeed, 0) * Time.deltaTime;
        }
        else if (_input.GetLaunch() && _ball.IsOnPad)
        {
            _launchBall.Invoke();
        }

        float x = Mathf.Clamp(transform.position.x, _leftEdge, _rightEdge);
        _position = new Vector3(x, transform.position.y);
        transform.position = _position;
    }

    private float GetLeftEdge()
    {
        float position = LeftWall.transform.position.x + LeftWallSpriteRenderer.bounds.extents.x + _spriteRenderer.bounds.extents.x;
        return position;
    }

    private float GetRightEdge()
    {
        float position = RightWall.transform.position.x - RightWallSpriteRenderer.bounds.extents.x - _spriteRenderer.bounds.extents.x;
        return position;
    }

    /// <summary>
    /// Event action
    /// </summary>
    public void Enlarge()
    {
        if (transform.localScale.x + _settings.EnlargeAmount < _settings.MaximumScale)
        {
            transform.localScale += new Vector3(_settings.EnlargeAmount, 0, 0);
        }
    }
}