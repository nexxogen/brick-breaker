﻿using UnityEngine;

public class PowerUpSpawner
{
    // Injected variables
    private LevelPrefabsContainer _prefabs;
    private PowerUpFactory _powerUpFactory;

    public PowerUpSpawner(LevelPrefabsContainer prefabs, PowerUpFactory factory)
    {
        _prefabs = prefabs;
        _powerUpFactory = factory;
    }

    public void Spawn(Vector3 position, float probability)
    {
        float randomFloat = Random.Range(0f, 1f);

        if (randomFloat <= probability)
        {
            _powerUpFactory.Prefab = _prefabs.PowerUps[Random.Range(0, _prefabs.PowerUps.Length)];
            PowerUp powerUp = _powerUpFactory.Create();
            powerUp.Launch(position);
        }
    }
}