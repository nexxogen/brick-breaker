﻿using UnityEngine;

public class KeyboardInputHandler : IInputHandler
{
    public bool GetLaunch()
    {
        return Input.GetKeyDown(KeyCode.Space);
    }

    public bool GetLeft()
    {
        return Input.GetKey(KeyCode.LeftArrow);
    }

    public bool GetRight()
    {
        return Input.GetKey(KeyCode.RightArrow);
    }
}
