﻿public interface IInputHandler
{
    bool GetLeft();
    bool GetRight();
    bool GetLaunch();
}